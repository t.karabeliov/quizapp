namespace QuizApp.Model
{
    public class Game
    {
        public virtual int Id { get; set; }

        public virtual int NumberOfPlayers { get; set; }

        public virtual string State { get; set;}
    }
}