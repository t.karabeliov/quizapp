using System.Text.Json.Serialization;

namespace QuizApp.Web.ViewModels
{
    public class LoginResult
    {
        [JsonPropertyName("username")]
        public string Username { get; set; }

        public string[] Roles  { get; set; }

        [JsonPropertyName("accesstoken")]
        public string AccessToken { get; set; }

        [JsonPropertyName("refreshtoken")]
        public string RefreshToken { get; set; }
    }
}