namespace QuizApp.Web.ViewModels
{
    public class RefreshTokenRequest
    {
        public string RefreshToken { get; set; }
    }
}