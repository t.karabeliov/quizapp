using Microsoft.Extensions.DependencyInjection;
using NHibernate.AspNetCore.Identity;
using NHibernate.AspNetCore.Identity.Mappings;
using NHibernate.Cfg;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Mapping.ByCode;
using QuizApp.Data;
using QuizApp.Model;
using QuizApp.Web.IdentityAuth;

namespace QuizApp.Web
{
    public static class NHibernateExtensions
    {
        public static IServiceCollection AddNHibernate(this IServiceCollection services, string connectionString)
        {
            var mapper = new ModelMapper();
    
            var configuration = new Configuration();
            configuration.DataBaseIntegration(c =>
            {
                c.Dialect<MsSql2012Dialect>();
                c.Driver<SqlClientDriver>();
                c.ConnectionString = connectionString;
                c.LogFormattedSql = true;
                c.LogSqlInConsole = true;
            });

            // configuration.AddIdentityMappingsForMsSql();

            mapper.AddMapping<GameMap>();
            mapper.AddMapping<IdentityRoleMappingMsSql>();
            mapper.AddMapping<IdentityRoleClaimMappingMsSql>();
            mapper.AddMapping<IdentityUserMappingMsSql>();
            mapper.AddMapping<IdentityUserClaimMappingMsSql>();
            mapper.AddMapping<IdentityUserLoginMappingMsSql>();
            mapper.AddMapping<IdentityUserRoleMappingMsSql>();
            mapper.AddMapping<IdentityUserTokenMappingMsSql>();
            mapper.AddMapping<ApplicationUserMap>();
            mapper.AddMapping<ApplicationRoleMap>();
            configuration.AddMapping(mapper.CompileMappingForAllExplicitlyAddedEntities());
    
            var sessionFactory = configuration.BuildSessionFactory();
    
            services.AddSingleton(sessionFactory);
            services.AddScoped(factory => sessionFactory.OpenSession());
            services.AddScoped(typeof(IMapperSession<>), typeof(NHibernateMapperSession<>));
    
            return services;
        }
    }
}