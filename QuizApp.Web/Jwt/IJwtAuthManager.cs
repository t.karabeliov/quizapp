using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace QuizApp.Web.Jwt
{
    public interface IJwtAuthManager
    {
        JwtAuthResult GenerateTokens(string username, List<Claim> claims, DateTime now);

        JwtAuthResult Refresh(string refreshToken, string accessToken, DateTime now);
    }
}