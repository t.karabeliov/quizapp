﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using QuizApp.Application;

namespace QuizApp.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GameController : ControllerBase
    {
        private readonly ILogger<GameController> _logger;
        private readonly IGameManager _gameManager;

        public GameController(ILogger<GameController> logger, IGameManager gameManager)
        {
            _logger = logger;
            _gameManager = gameManager;
        }

        [HttpGet]
        [Authorize]
        [Route("find-game")]
        public async Task FindGame(CancellationToken cancellationToken)
        {
            var game = await _gameManager.FindActiveGame();
            if (game == null)
            {
                await _gameManager.CreateGame(cancellationToken);
            }
        }

        [HttpGet]
        [Authorize]
        public async Task Get(int gameId)
        {
            
        }
    }
}
