using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using QuizApp.Web.IdentityAuth;
using QuizApp.Web.Jwt;
using QuizApp.Web.ViewModels;

namespace QuizApp.Web
{
    public class AuthenticationController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IJwtAuthManager _jwtAuthManager;

        public AuthenticationController(UserManager<ApplicationUser> userManager, IConfiguration configuration, IJwtAuthManager jwtAuthManager)
        {
            _userManager = userManager;
            _jwtAuthManager = jwtAuthManager;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; set; }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            var userExists = _userManager.Users.FirstOrDefault(u => u.UserName == model.Username);
            if (userExists != null)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new Response { Status = "Error", Message = "User already exists!" });
            }
            
            var user = new ApplicationUser()
            {
                UserName = model.Username,
                Email = model.Email
            };

            var result = await _userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "User creation failed! Please check user details and try again." });
            }

            return Ok(new Response { Status = "Success", Message = "User created successfully!" });
        }

        [HttpPost]  
        [Route("login")]  
        public async Task<IActionResult> Login([FromBody] LoginModel model)  
        {
            var user = await _userManager.FindByNameAsync(model.Username);  
            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))  
            {  
                var claims = new List<Claim>
                {
                    new Claim("username", model.Username),
                };

                var roles = await _userManager.GetRolesAsync(user);

                if (roles != null)
                {
                    foreach (var role in roles)
                    {
                        claims.Add(new Claim("role", role));
                    }
                }

                var jwtResult = _jwtAuthManager.GenerateTokens(model.Username, claims, DateTime.Now);
  
                return Ok(new LoginResult
                {
                    Username = model.Username,
                    Roles = roles?.ToArray(),
                    AccessToken = jwtResult.AccessToken,
                    RefreshToken = jwtResult.RefreshToken.TokenString
                });
            }

            return Unauthorized();  
        }

        [HttpPost("refresh-token")]
        [Authorize]
        public async Task<ActionResult> RefreshToken([FromBody] RefreshTokenRequest request)
        {
            try
            {
                var userName = User.Identity.Name;

                if (string.IsNullOrWhiteSpace(request.RefreshToken))
                {
                    return Unauthorized();
                }

                var accessToken = await HttpContext.GetTokenAsync("Bearer", "access_token");
                var jwtResult = _jwtAuthManager.Refresh(request.RefreshToken, accessToken, DateTime.Now);
                return Ok(new LoginResult
                {
                    Username = userName,
                    Roles = User.FindAll(ClaimTypes.Role).Select(c => c.Value).ToArray(),
                    AccessToken = jwtResult.AccessToken,
                    RefreshToken = jwtResult.RefreshToken.TokenString
                });
            }
            catch (SecurityTokenException e)
            {
                return Unauthorized(e.Message); // return 401 so that the client side can redirect the user to login page
            }
        }
    }
}