using NHIdentityRole = NHibernate.AspNetCore.Identity.IdentityRole;

namespace QuizApp.Web.IdentityAuth
{
    public class ApplicationRole : NHIdentityRole
    {
        public virtual string Description { get; set; }
    }
}