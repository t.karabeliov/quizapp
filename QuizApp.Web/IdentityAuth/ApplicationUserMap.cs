using NHibernate.Mapping.ByCode.Conformist;

namespace QuizApp.Web.IdentityAuth
{
    public class ApplicationUserMap : JoinedSubclassMapping<ApplicationUser>
    {
        public ApplicationUserMap()
        {
            Key(u => u.Column("Id"));
            Property(u => u.LoginCount);
            Table("ApplicationUser");
        }
    }
}