using NHibernate.Mapping.ByCode.Conformist;

namespace QuizApp.Web.IdentityAuth
{
    public class ApplicationRoleMap : JoinedSubclassMapping<ApplicationRole>
    {
        public ApplicationRoleMap()
        {
            Key(u => u.Column("Id"));
            Property(u => u.Description);
            Table("ApplicationRole");
        }
    }
}