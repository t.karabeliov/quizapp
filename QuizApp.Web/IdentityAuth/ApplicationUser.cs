using System;
using Microsoft.AspNetCore.Identity;
using NHIdentityUser = NHibernate.AspNetCore.Identity.IdentityUser;

namespace QuizApp.Web.IdentityAuth
{
    public class ApplicationUser : NHIdentityUser
    {
        public virtual int LoginCount { get; set; }
    }
}