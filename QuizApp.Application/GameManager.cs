using System.Threading.Tasks;
using System.Linq;
using QuizApp.Data;
using QuizApp.Model;
using System;
using System.Threading;
using NHibernate.Linq;

namespace QuizApp.Application
{
    public class GameManager : IGameManager
    {
        private readonly IMapperSession<Game> _gameSession;

        public GameManager(IMapperSession<Game> gameSession)
        {
            _gameSession = gameSession;
        }

        public async Task<Game> FindActiveGame()
        {
            return await _gameSession.All.FirstOrDefaultAsync(g => g.State == "Pending");
        }

        public async Task<Game> GetGame(int id)
        {
            return await _gameSession.All.FirstOrDefaultAsync(g => g.Id == id);
        }

        public async Task CreateGame(CancellationToken cancellationtoken)
        {   
            await _gameSession.Add(new Game { NumberOfPlayers = 1, State = "Pending" }, cancellationtoken);
        }
    }
}