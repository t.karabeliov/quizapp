using System.Threading;
using System.Threading.Tasks;
using QuizApp.Model;

namespace QuizApp.Application
{
    public interface IGameManager
    {
        Task<Game> FindActiveGame();

        Task CreateGame(CancellationToken cancellationToken);
    }
}