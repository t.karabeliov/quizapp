using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using QuizApp.Model;

namespace QuizApp.Data
{
    public class GameMap : ClassMapping<Game>
    {
        public GameMap()
        {
            Id(x => x.Id, m => m.Generator(Generators.Native));
            Property(x => x.NumberOfPlayers);
            Property(x => x.State);
            Table("Game");
        }
    }
}