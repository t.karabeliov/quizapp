CREATE TABLE [dbo].[Game]
(
   [Id] INT NOT NULL PRIMARY KEY IDENTITY(1, 1),

   [NumberOfPlayers] INT NOT NULL,

   [DateCreated] DATETIME2 DEFAULT (GETDATE()),

   [State] varchar(50) NOT NULL DEFAULT 'Pending',
)

insert into game values ('a')

drop table game

select * from Game

delete from game

select * from sys.tables

select * from sys.columns
where object_id = '917578307'

select * from aspnetusers

select * from applicationuser

drop table applicationuser

CREATE TABLE [dbo].[ApplicationUser]
(
   Id nvarchar(32) not null,

   [LoginCount] INT null,

   constraint FK_AppUsers_AspNetUsers
    foreign key (Id) references AspNetUsers (Id)
    on update cascade
    on delete cascade
)

create table ApplicationRole (
  Id nvarchar(32) not null,
  Description nvarchar(256) not null,
  constraint PK_AppRoles primary key (Id),
  constraint FK_AppRoles_AspNetRoles
  foreign key (Id) references AspNetRoles (Id)
    on update cascade
    on delete cascade
)