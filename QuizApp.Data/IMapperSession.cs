using System.Threading.Tasks;
using System.Linq;
using QuizApp.Model;
using System.Threading;

namespace QuizApp.Data
{
    public interface IMapperSession<T>
    {
        IQueryable<T> All { get; }

        Task Add(T item, CancellationToken cancellationToken);
    }
}