using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NHibernate;
using QuizApp.Model;

namespace QuizApp.Data
{
    public class NHibernateMapperSession<T> : IMapperSession<T>
    {
        private readonly ISession _session;
    
        public NHibernateMapperSession(ISession session)
        {
            _session = session;
        }
    
        public IQueryable<T> All => _session.Query<T>();

        public async Task Add(T item, CancellationToken cancellationToken)
        {
            await _session.SaveAsync(item, cancellationToken);
            await _session.FlushAsync();
        }
    }
}